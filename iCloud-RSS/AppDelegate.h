//
//  AppDelegate.h
//  iCloud-RSS
//
//  Created by Gabriel Torrecillas Sartori on 20/05/14.
//  Copyright (c) 2014 None. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
